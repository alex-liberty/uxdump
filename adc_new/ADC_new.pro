#-------------------------------------------------
#
# Project created by QtCreator 2016-03-07T19:18:55
#
#-------------------------------------------------

QT       += core gui
QT       += serialport
CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = ADC_new
TEMPLATE = app


SOURCES += main.cpp\
        adc_new.cpp \
    qcustomplot.cpp

HEADERS  += adc_new.h \
    qcustomplot.h

FORMS    += adc_new.ui
