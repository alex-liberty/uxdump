#include "adc_new.h"
#include "ui_adc_new.h"
#include <QMessageBox>
#include <QDateTime>
#define CONFIG_FILE "adc_new.cfg"

ADC_new::ADC_new(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ADC_new)
{
    ui->setupUi(this);
    readConfig();
    CreateGUI();
    SetupPlot();
    connect(&dataTimer, SIGNAL(timeout()), this, SLOT(replot()));



    // setup a socket: handler of the unix signal will use it to activate QT signal in case of SIGTERM
    if (::socketpair(AF_UNIX, SOCK_STREAM, 0, sigtermFd))
        qFatal("couldn't create TERM socketpair");
    snTerm = new QSocketNotifier(sigtermFd[1],QSocketNotifier::Read, this); // it will help to fire QT event at SIGTERM
    connect(snTerm, SIGNAL(activated(int)), this, SLOT(handleSigTerm()));
    if (auto_mode)
    {
        on_Button_Connect_clicked();
        on_Button_Start_Log_clicked();
    }
}

void ADC_new::readConfig()
{
    QFile file(CONFIG_FILE);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) qDebug() << "file not opened";

    QByteArray content = file.readAll();
    //qDebug() << content;
    QJsonDocument jdoc= QJsonDocument::fromJson(content);
    QJsonObject obj = jdoc.object();
    auto_mode=(obj["mode"].toString()=="auto")?1:0;
    portName=obj["port"].toString();

    file.close();
}

void ADC_new::writeConfig()
{
    QFile file(CONFIG_FILE);
    if (!file.open(QIODevice::WriteOnly)) qDebug() << "unable to save config file";

    QJsonObject obj;
    obj["mode"] = (auto_mode)?"auto":"interactive";
    obj["port"] = portName;

    QJsonDocument jdoc(obj);

    file.write(jdoc.toJson());
    file.close();
}

void ADC_new::CreateGUI()
{
    if(QSerialPortInfo::availablePorts().size() == 0)
    {                                                   // Check if there are any ports at all; if not, disable controls and return
        ui->statusBar->showMessage("Device is not connected.");
        QMessageBox::critical(this, "Error","Device is not connected. Please, try to connect it one more time or reload the program.");
        ui->comboBox_Port->setEnabled(false);
        //enableControls(false);
        ui->Button_Connect->setEnabled(false);
        return;
    }

    for(QSerialPortInfo port : QSerialPortInfo::availablePorts())       // List all available serial ports and populate ports combo box
    {
        ui->comboBox_Port->addItem(port.portName());
    }
    ui->comboBox_Port->setCurrentIndex(ui->comboBox_Port->findText(portName));
    ui->Button_Connect->setEnabled(true);
    ui->Button_Abort->setEnabled(false);
    ui->Button_Start_Log->setEnabled(false);
    ui->Button_Stop_Log->setEnabled(false);

    clear_LCD();
}

void ADC_new::clear_LCD()
{
    ui->lab_LCD_GSR->setText("---");
    ui->lab_LCD_HR1->setText("---");
    ui->lab_LCD_HR2->setText("---");
    ui->lab_LCD_PTT->setText("---");
}

void ADC_new::fill_LCD()
{
    ui->lab_LCD_HR1->setText(QString::number(BPM));
    ui->lab_LCD_HR2->setText(QString::number(BPM_2));
    ui->lab_LCD_GSR->setText(QString::number(GSR));
    ui->lab_LCD_PTT->setText(QString::number(PTT));
}

/*
void ADC_new::EnableControls(bool enable)
{
    ui->comboBox_Port->setEnabled(enable);
}
*/


void ADC_new::SetupPlot()
{
    /*
    QPen pen_HR;
    pen_HR.setWidth(3);
    pen_HR.setColor(Qt::red);
    QPen pen_GSR;
    pen_GSR.setWidth(3);
    pen_GSR.setColor(Qt::blue);
    */

    ui->cust_plot_heart_rate_1->clearItems();
    ui->cust_plot_heart_rate_1->addGraph();
    ui->cust_plot_heart_rate_1->setNotAntialiasedElements(QCP::aeAll);                                      // used for higher performance (see QCustomPlot real time example)

    ui->cust_plot_heart_rate_2->clearItems();
    ui->cust_plot_heart_rate_2->addGraph();
    ui->cust_plot_heart_rate_2->setNotAntialiasedElements(QCP::aeAll);

    ui->cust_plot_GSR->clearItems();
    ui->cust_plot_GSR->addGraph();
    ui->cust_plot_GSR->setNotAntialiasedElements(QCP::aeAll);

    ui->cust_plot_heart_rate_1->graph(0)->setPen(QPen(Qt::red));
    ui->cust_plot_heart_rate_2->graph(0)->setPen(QPen(Qt::red));

    /*
    ui->cust_plot_heart_rate_1->graph(0)->setPen(pen_HR);
    ui->cust_plot_heart_rate_2->graph(0)->setPen(pen_HR);
    ui->cust_plot_GSR->graph(0)->setPen(pen_GSR);
    */

    //ui->cust_plot_heart_rate_1->graph(0)->setBrush(QBrush(QColor(0, 0, 255, 20)));


    QFont font("Arial", 10);
    font.setStyleStrategy(QFont::NoAntialias);
    ui->cust_plot_heart_rate_1->xAxis->setTickLabelFont(font);
    ui->cust_plot_heart_rate_1->yAxis->setTickLabelFont(font);
    ui->cust_plot_heart_rate_1->legend->setFont(font);
    ui->cust_plot_heart_rate_1->xAxis->setLabelFont(font);
    ui->cust_plot_heart_rate_1->yAxis->setLabelFont(font);

    ui->cust_plot_heart_rate_2->xAxis->setTickLabelFont(font);
    ui->cust_plot_heart_rate_2->yAxis->setTickLabelFont(font);
    ui->cust_plot_heart_rate_2->legend->setFont(font);
    ui->cust_plot_heart_rate_2->xAxis->setLabelFont(font);
    ui->cust_plot_heart_rate_2->yAxis->setLabelFont(font);

    ui->cust_plot_GSR->xAxis->setTickLabelFont(font);
    ui->cust_plot_GSR->yAxis->setTickLabelFont(font);
    ui->cust_plot_GSR->legend->setFont(font);
    ui->cust_plot_GSR->xAxis->setLabelFont(font);
    ui->cust_plot_GSR->yAxis->setLabelFont(font);

    //ui->heart_rate_1->xAxis->setLabel("Counts");
    //ui->heart_rate_1->yAxis->setLabel("Voltage");
    ui->cust_plot_heart_rate_1->yAxis->setAutoTickStep(true);
    //ui->cust_plot_heart_rate_1->yAxis->setAutoTickStep(false);
    //ui->cust_plot_heart_rate_1->yAxis->setRange(0, 1000);
    //ui->cust_plot_heart_rate_1->yAxis->setTickStep(100);
    ui->cust_plot_heart_rate_1->xAxis->setRange(0, NUMBER_OF_POINTS);
    //ui->cust_plot_heart_rate_1->xAxis->setTickStep(5);

    ui->cust_plot_heart_rate_2->yAxis->setAutoTickStep(true);
    //ui->cust_plot_heart_rate_2->yAxis->setAutoTickStep(false);
    //ui->cust_plot_heart_rate_2->yAxis->setRange(0, 1000);
    //ui->cust_plot_heart_rate_2->yAxis->setTickStep(200);
    ui->cust_plot_heart_rate_2->xAxis->setRange(0, NUMBER_OF_POINTS);
    //ui->cust_plot_heart_rate_2->xAxis->setTickStep(5);

    //ui->cust_plot_GSR->yAxis->setAutoTickStep(false);
    ui->cust_plot_GSR->yAxis->setAutoTickStep(true);
    //ui->cust_plot_GSR->yAxis->setRange(0, 1000);
    //ui->cust_plot_GSR->yAxis->setTickStep(25);
    ui->cust_plot_GSR->xAxis->setRange(0, NUMBER_OF_POINTS);
    //ui->cust_plot_GSR->xAxis->setTickStep(5);

    // Set initial tick step
    ui->cust_plot_heart_rate_1->xAxis->grid()->setPen(QPen(QColor(170,170,170), 1, Qt::DotLine));
    ui->cust_plot_heart_rate_1->yAxis->grid()->setPen(QPen(QColor(170,170,170), 1, Qt::DotLine));
    ui->cust_plot_heart_rate_1->xAxis->grid()->setSubGridPen(QPen(QColor(80,80,80), 1, Qt::DotLine));
    ui->cust_plot_heart_rate_1->xAxis->grid()->setSubGridVisible(true);

    ui->cust_plot_heart_rate_2->xAxis->grid()->setPen(QPen(QColor(170,170,170), 1, Qt::DotLine));
    ui->cust_plot_heart_rate_2->yAxis->grid()->setPen(QPen(QColor(170,170,170), 1, Qt::DotLine));
    ui->cust_plot_heart_rate_2->xAxis->grid()->setSubGridPen(QPen(QColor(80,80,80), 1, Qt::DotLine));
    ui->cust_plot_heart_rate_2->xAxis->grid()->setSubGridVisible(true);

    ui->cust_plot_GSR->xAxis->grid()->setPen(QPen(QColor(170,170,170), 1, Qt::DotLine));
    ui->cust_plot_GSR->yAxis->grid()->setPen(QPen(QColor(170,170,170), 1, Qt::DotLine));
    ui->cust_plot_GSR->xAxis->grid()->setSubGridPen(QPen(QColor(80,80,80), 1, Qt::DotLine));
    ui->cust_plot_GSR->xAxis->grid()->setSubGridVisible(true);

    ui->cust_plot_heart_rate_1->xAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
    ui->cust_plot_heart_rate_1->yAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);

    ui->cust_plot_heart_rate_2->xAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
    ui->cust_plot_heart_rate_2->yAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);

    ui->cust_plot_GSR->xAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
    ui->cust_plot_GSR->yAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);


}

ADC_new::~ADC_new()
{
    if(UXDump->isOpen())
    {
        UXDump->close(); //    Close the serial port if it's open.
    }
    delete ui;
}

void ADC_new::readSerial()
{
        QStringList incomingData,incomingData_BPM, not_raw_Data;
        serialData = UXDump->readAll();
        char *temp_signals = serialData.data();
        for (int k = 0; temp_signals[k] != '\0'; k++)
        {
            if (temp_signals[k] == 'S')
            {
                receivedData.clear();
            }

            else if (isdigit(temp_signals[k]) || isspace(temp_signals[k]))
            {
                receivedData.append(temp_signals[k]);
            }

            else if (temp_signals[k] == '.')
            {
                    incomingData = receivedData.split(' ');
                    //qDebug() << incomingData[0] << incomingData[1] << incomingData[2];
                    HR1 = incomingData[0].toInt();
                    HR2 = incomingData[1].toInt();
                    GSR = incomingData[2].toInt();
                    BPM = incomingData[3].toInt();
                    BPM_2 = incomingData[4].toInt();
                    PTT = incomingData[5].toInt();
            }
        }

        plot();
}

void ADC_new::update_LCD_NUM(QString sensor_reading)
{
    ui->lab_LCD_HR1->setText(sensor_reading);
}

void ADC_new::on_Button_Connect_clicked()
{   portName=ui->comboBox_Port->currentText();
    writeConfig();
    if(connected)
    {                                                                       // If application is connected, disconnect
        dataPointNumber = 0;
        UXDump->close();                                                              // Close serial port
        //emit portClosed();                                                                // Notify application
        delete UXDump;                                                                // Delete the pointer
        UXDump = NULL;                                                                // Assign NULL to dangling pointer
        ui->Button_Connect->setText("Connect");                                            // Change Connect button text, to indicate disconnected
        ui->statusBar->showMessage("Disconnected.");                                      // Show message in status bar
        connected = false;                                                                // Set connected status flag to false
        plotting = false;                                                                 // Not plotting anymore
		serialData.clear();
        receivedData.clear();                                                             // Clear received string
        ui->comboBox_Port->setEnabled(true);
        //enableControls(true);
        ui->Button_Refresh->setEnabled(true);
        ui->Button_Abort->setEnabled(false);
        ui->Button_Start_Log->setEnabled(false);
        ui->Button_Stop_Log->setEnabled(false);
        clear_LCD();
        BPM = 0;
        BPM_2 = 0;
        GSR = 0;
        PTT = 0;
        LOGNAME = "";
        log = false;
//------------------------------
        dataTimer.stop();
//------------------------------
    }
    else
    {
        ui->cust_plot_heart_rate_1->clearItems();
        //ui->cust_plot_heart_rate_1->yAxis->setRange(0, 1000);
        //ui->cust_plot_heart_rate_1->yAxis->setTickStep(200);
        ui->cust_plot_heart_rate_1->xAxis->setRange(0, NUMBER_OF_POINTS);

        ui->cust_plot_heart_rate_2->clearItems();
        //ui->cust_plot_heart_rate_2->yAxis->setRange(0, 1000);
        //ui->cust_plot_heart_rate_2->yAxis->setTickStep(200);
        ui->cust_plot_heart_rate_2->xAxis->setRange(0, NUMBER_OF_POINTS);

        ui->cust_plot_GSR->clearItems();
        //ui->cust_plot_GSR->yAxis->setRange(0, 1000);
        //ui->cust_plot_GSR->yAxis->setTickStep(25);
        ui->cust_plot_GSR->xAxis->setRange(0, NUMBER_OF_POINTS);

        connected = true;
        plotting = true;
        QSerialPortInfo portInfo(ui->comboBox_Port->currentText());
        UXDump = new QSerialPort(portInfo, 0);
        UXDump->open(QIODevice::ReadWrite);
        UXDump->setBaudRate(QSerialPort::Baud9600);
        UXDump->setDataBits(QSerialPort::Data8);
        UXDump->setParity(QSerialPort::NoParity);
        UXDump->setStopBits(QSerialPort::OneStop);
        UXDump->setFlowControl(QSerialPort::NoFlowControl);
        ui->statusBar->showMessage("Connected to device on " + ui->comboBox_Port->currentText());
        ui->Button_Connect->setText("Disconnect");
        ui->Button_Refresh->setEnabled(false);
        ui->Button_Abort->setEnabled(false);
        ui->Button_Start_Log->setEnabled(true);
        ui->comboBox_Port->setEnabled(false);
        //enableControls(false);
        QObject::connect(UXDump, SIGNAL(readyRead()), this, SLOT(readSerial()));
        QObject::connect(UXDump, SIGNAL(readyRead()), this, SLOT(logging()));
        //QObject::connect(UXDump, SIGNAL(readyRead()), this, SLOT(on_Button_Start_Log_clicked()));

//------------------------------
        dataTimer.start(150);
//------------------------------
    }
}

void ADC_new::clearControls()
{
    ui->comboBox_Port->clear();
}

void ADC_new::plot()
{
    ui->cust_plot_heart_rate_1->clearItems();                                                              // Remove everything from the plot
    ui->cust_plot_heart_rate_2->clearItems();
    ui->cust_plot_GSR->clearItems();
    dataPointNumber++;

    QFont legendFont = font();
    legendFont.setPointSize(9);

    ui->cust_plot_heart_rate_1->legend->setVisible(true);
    ui->cust_plot_heart_rate_1->legend->setFont(legendFont);
    ui->cust_plot_heart_rate_1->legend->setBrush(QBrush(QColor(255,255,255,230)));
    ui->cust_plot_heart_rate_1->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignBottom|Qt::AlignRight);
    ui->cust_plot_heart_rate_1->graph(0)->setName("Heart Rate 1");
    ui->cust_plot_heart_rate_1->graph(0)->addData(dataPointNumber, HR1);
    ui->cust_plot_heart_rate_1->graph(0)->removeDataBefore(dataPointNumber - NUMBER_OF_POINTS);
    ui->cust_plot_heart_rate_1->graph(0)->rescaleValueAxis();

    ui->cust_plot_heart_rate_2->legend->setVisible(true);
    ui->cust_plot_heart_rate_2->legend->setFont(legendFont);
    ui->cust_plot_heart_rate_2->legend->setBrush(QBrush(QColor(255,255,255,230)));
    ui->cust_plot_heart_rate_2->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignBottom|Qt::AlignRight);
    ui->cust_plot_heart_rate_2->graph(0)->setName("Heart Rate 2");
    ui->cust_plot_heart_rate_2->graph(0)->addData(dataPointNumber, HR2);
    ui->cust_plot_heart_rate_2->graph(0)->removeDataBefore(dataPointNumber - NUMBER_OF_POINTS);
    ui->cust_plot_heart_rate_2->graph(0)->rescaleValueAxis();

    ui->cust_plot_GSR->legend->setVisible(true);
    ui->cust_plot_GSR->legend->setFont(legendFont);
    ui->cust_plot_GSR->legend->setBrush(QBrush(QColor(255,255,255,230)));
    ui->cust_plot_GSR->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignBottom|Qt::AlignRight);
    ui->cust_plot_GSR->graph(0)->setName("GSR");
    ui->cust_plot_GSR->graph(0)->addData(dataPointNumber, GSR);
    ui->cust_plot_GSR->graph(0)->removeDataBefore(dataPointNumber - NUMBER_OF_POINTS);
    ui->cust_plot_GSR->graph(0)->rescaleValueAxis();
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
    fill_LCD();
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------

    connect(ui->cust_plot_heart_rate_1->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->cust_plot_heart_rate_1->xAxis2, SLOT(setRange(QCPRange)));
    connect(ui->cust_plot_heart_rate_2->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->cust_plot_heart_rate_2->xAxis2, SLOT(setRange(QCPRange)));
    connect(ui->cust_plot_GSR->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->cust_plot_GSR->xAxis2, SLOT(setRange(QCPRange)));

}


void ADC_new::replot()
{
    if(connected)
    {
        ui->cust_plot_heart_rate_1->xAxis->setRange(dataPointNumber - NUMBER_OF_POINTS, dataPointNumber);
        ui->cust_plot_heart_rate_1->replot();

        ui->cust_plot_heart_rate_2->xAxis->setRange(dataPointNumber - NUMBER_OF_POINTS, dataPointNumber);
        ui->cust_plot_heart_rate_2->replot();

        ui->cust_plot_GSR->xAxis->setRange(dataPointNumber - NUMBER_OF_POINTS, dataPointNumber);
        ui->cust_plot_GSR->replot();
    }
}

void ADC_new::on_Button_Refresh_clicked()
{
    if(QSerialPortInfo::availablePorts().size() == 0)
    {
        ui->statusBar->showMessage("Device is not connected.");
        QMessageBox::critical(this, "Error","Device is not connected. Please, push the Refresh port button or try to connect device one more time or reload the program.");
        clearControls();
        ui->comboBox_Port->setEnabled(false);
        //enableControls(false);
        ui->Button_Connect->setEnabled(false);
        return;
    }

    for(QSerialPortInfo port : QSerialPortInfo::availablePorts())       // List all available serial ports and populate ports combo box
    {
        ui->comboBox_Port->clear();

        Q_FOREACH(QSerialPortInfo port, QSerialPortInfo::availablePorts())
        {
             ui->comboBox_Port->addItem(port.portName());
        }
    }
    ui->comboBox_Port->setEnabled(true);
    //enableControls(true);
    ui->Button_Connect->setEnabled(true);
}

void ADC_new::on_Button_Start_Log_clicked()
{
    ui->Button_Stop_Log->setEnabled(true);
    ui->Button_Abort->setEnabled(true);

    last_timestamp=QDateTime::currentDateTime();
    accumulated_GSR=accumulated_BPM=accumulated_BPM_2=accumulated_PTT=number_of_accumulated_values=0;
    log = true;
    ui->Button_Start_Log->setEnabled(false);
   // QString filename = "F:/QTProjects/ADC_new/ADC_new/output (" + QDateTime::currentDateTime().toString("dd-MM-yyyy_hh-mm") + ").txt";
    QString filename = QDir::currentPath()+"/Result/";
    filename.append(QDateTime::currentDateTime().toString("dd_MM_yyyy_HH_mm_ss"));
    filename.append(".adc_new.csv");
    LOGNAME = filename;
    QFile file(filename);
    file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append);
    QTextStream out(&file);
    out << "Date,Time,GSR,BPM,BPM_2,PTT\n";
    file.close();
}


void ADC_new::logging()
{
    if (log == true)
    {
        if (last_timestamp.msecsTo(QDateTime::currentDateTime())<250)
        {
          //averagind log data
          accumulated_GSR+=GSR; accumulated_BPM+=BPM; accumulated_BPM_2+=BPM_2; accumulated_PTT+=PTT;
          number_of_accumulated_values++;
        }
        else
        { // writing log data and resetting last_timestamp
           // qDebug() << last_timestamp.msecsTo(QDateTime::currentDateTime());
             accumulated_GSR/=number_of_accumulated_values;
             accumulated_BPM/=number_of_accumulated_values;
             accumulated_BPM_2/=number_of_accumulated_values;
             accumulated_PTT/=number_of_accumulated_values;

              QFile file(LOGNAME);
              file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append);
              QTextStream out(&file);
              out << QDateTime::currentDateTime().toString("dd.MM.yyyy,hh:mm:ss:zzz") << ","
                  << accumulated_GSR << "," << accumulated_BPM << "," << accumulated_BPM_2 << ","
                  << accumulated_PTT << "\n";
              accumulated_GSR=GSR; accumulated_BPM=BPM; accumulated_BPM_2=BPM_2; accumulated_PTT=PTT;
              number_of_accumulated_values=1; last_timestamp=QDateTime::currentDateTime();
        }
    }
    else
    {
    }
}

void ADC_new::on_Button_Stop_Log_clicked()
{
    ui->Button_Start_Log->setEnabled(true);
    ui->Button_Stop_Log->setEnabled(false);

    log = false;
}


void ADC_new::on_Button_Abort_clicked()
{
    QDir dir;
    /*
    dataPointNumber = 0;
    UXDump->close();                                                              // Close serial port
    delete UXDump;                                                                // Delete the pointer
    UXDump = NULL;                                                                // Assign NULL to dangling pointer
    ui->Button_Connect->setText("Connect");                                            // Change Connect button text, to indicate disconnected
    ui->statusBar->showMessage("Disconnected.");                                      // Show message in status bar
    connected = false;                                                                // Set connected status flag to false
    plotting = false;                                                                 // Not plotting anymore
    serialData.clear();
    receivedData.clear();                                                             // Clear received string
    ui->comboBox_Port->setEnabled(true);
    //enableControls(true);
    ui->Button_Refresh->setEnabled(true);
    ui->Button_Abort->setEnabled(false);
    ui->Button_Start_Log->setEnabled(false);
    clear_LCD();
    BPM = 0;
    BPM_2 = 0;
    GSR = 0;
    PTT = 0;
    */
    dir.remove(LOGNAME);
    LOGNAME = "";
    ui->Button_Start_Log->setEnabled(true);
    ui->Button_Stop_Log->setEnabled(false);
    ui->Button_Abort->setEnabled(false);

    //log = false;
}

void ADC_new::termSignalHandler(int)
{
    char a = 1;
    ::write(sigtermFd[0], &a, sizeof(a));
}

void ADC_new::handleSigTerm()
{
    snTerm->setEnabled(false);
    char tmp;
    ::read(sigtermFd[1], &tmp, sizeof(tmp));
    close(); // making close event in QT-noticeable way
    snTerm->setEnabled(true);
}

void ADC_new::closeEvent(QCloseEvent *event)
{
    exit(0);
}
