#include "keyboardtest.h"
#include "ui_keyboardtest.h"


keyboardTest::keyboardTest(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::keyboardTest)
{
    ui->setupUi(this);
    startCreate();
    QString dirName = "Reports";
    bool isExsist = QDir(QDir::currentPath() + QDir::separator() + dirName ).exists();
    QDir dir = QDir( QDir::currentPath() );

    if( !isExsist )
        isExsist = dir.mkdir( dirName );
}

keyboardTest::~keyboardTest()
{
    QFile("output").remove();
    QFile("input").remove();

    delete ui;
}

void keyboardTest::startCreate()
{
    QString path = "./ubuntu";

    pathReport = QDir::currentPath() + "/Reports/";
    pathReport .append(QDateTime::currentDateTime().toString("dd_MM_yyyy_HH_mm_ss"));
    pathReport.append(".ubuntuKeyboard");
    pathReport.append(".csv");

    QFile file(path);

    if ( !file.open( QIODevice::ReadOnly | QIODevice::Text ) ) {
        qDebug() << "Error open file";
        return;
    }

    textFile = file.readAll();
    file.close();

    QFile fileReport(pathReport);
    if ( !fileReport.open( QIODevice::Append | QIODevice::Text) ) {
        qDebug() << "Error open file";
        return;
    }

    QTextStream outReport(&fileReport);
    outReport << "TimeStart,Duration,Simplicity, Count errors, Count output, Count input\n";

    fileReport.close();

    fileName = QDir::currentPath() + "/Result/" + QDateTime::currentDateTime().toString("dd_MM_yyyy_HH_mm_ss") +
            ".ubuntuKeyboard.csv";
    QFile fileLog(fileName);

    if ( !fileLog.open( QIODevice::Append | QIODevice::Text ) ) {
        qDebug() << "Error open file";
        return;
    }

    QTextStream out(&fileLog);
    out << "timeStart,Count errors\n";

    fileLog.close();


    paragraphs = textFile.split( "\n\n" );
    foreach (QString str, paragraphs) {
        str.simplified();
    }
    ui->labelOutputText->setText( paragraphs[ count++ ] );

    timeStart = QTime::currentTime();
    timeStamp = timeStart.toString("hh:mm:ss:zzz");
    //qDebug() << timeStart;
}


void keyboardTest::on_pushButton_clicked()
{
    proc = new QProcess();
    QString inputText2;
    QString outputText2;
    inputText = ui->textEdit->toPlainText();
    countInput = inputText.count();
    inputText.simplified();

    inputText.replace( QRegularExpression( " " ), "\n" );
    outputText = ui->labelOutputText->text();
    countOutput = outputText.count();
    outputText.simplified();
    outputText.replace( QRegularExpression( " " ), "\n" );

    foreach (QChar sim, inputText) {
        inputText2 += sim;
        inputText2 += "\n";
    }

    foreach (QChar sim, outputText) {
        outputText2 += sim;
        outputText2 += "\n";
    }
    QFile inputFile("input");

    if ( !inputFile.open( QIODevice::WriteOnly | QIODevice::Text) ) {
        return;
    }
    QTextStream in(&inputFile);
    in << inputText;

    inputFile.close();

    QFile outputFile("output");

    if ( !outputFile.open( QIODevice::WriteOnly | QIODevice::Text ) ){
        return;
    }
    QTextStream out(&outputFile);
    out << outputText;

    outputFile.close();

    ui->textEdit->setText( "" );


    proc->start( diff );

    if( !proc->waitForStarted() || !proc->waitForFinished() )
        return;

    QString diffOutput = proc->readAllStandardOutput();

    j = 0;
    countError = 0;
    while ((j = diffOutput.indexOf(">", j)) != -1) {
        countError++;
        ++j;
    }
    int countError2;
    int m = 0;
    while ((m = diffOutput.indexOf("<", m)) != -1) {
        countError2++;
        ++m;
    }

    QFile fileLog(fileName);
    if ( !fileLog.open( QIODevice::Append | QIODevice::Text ) ) {
        qDebug() << "Error open file";
        return;
    }

    QTextStream outLog(&fileLog);
    outLog << timeStamp << "," << countError << "\n";

    fileLog.close();
    //   qDebug() << countError;

    timeFinish = QTime::currentTime();
    duration = timeStart.msecsTo( timeFinish );

    if ( count == 1 )
        simplicity = 3;

    if ( count == 2 )
        simplicity = 2;

    if ( count == 3 || count == 4 )
        simplicity = 1;

    QFile fileReport(pathReport);
    if ( !fileReport.open( QIODevice::Append | QIODevice::Text) ) {
        qDebug() << "Error open file";
        return;
    }

    QTextStream outReport(&fileReport);
    outReport << timeStamp + "," + QString::number( duration ) + "," + QString::number( simplicity ) + "," +
                 QString::number( countError ) + "," + QString::number( countOutput ) +
                 "," + QString::number(countInput ) + "\n";

    fileReport.close();

    proc->terminate();

    if ( count < paragraphs.count())
        ui->labelOutputText->setText( paragraphs[ count++ ] );

    else
        this->close();


    timeStart = QTime::currentTime();
    timeStamp = timeStart.toString("hh:mm:ss:zzz");
}
