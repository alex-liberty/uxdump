#-------------------------------------------------
#
# Project created by QtCreator 2016-05-28T21:59:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = start_ubuntuKeyboard
TEMPLATE = app


SOURCES += main.cpp\
        keyboardtest.cpp

HEADERS  += keyboardtest.h

FORMS    += keyboardtest.ui
