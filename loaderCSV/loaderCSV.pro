#-------------------------------------------------
#
# Project created by QtCreator 2016-03-08T18:38:45
#
#-------------------------------------------------

QT       += sql

TARGET = loaderCSV
TEMPLATE = lib

DEFINES += LOADERCSV_LIBRARY

SOURCES += loadercsv.cpp

HEADERS += loadercsv.h\
        loadercsv_global.h

QMAKE_CXXFLAGS_CXX11    = -std=c++11

unix {
    target.path = /usr/lib
    INSTALLS += target
    myheaders.path = /usr/include
    myheaders.files = *.h
    INSTALLS += myheaders

#permissions for output files
    QMAKE_INSTALL_PROGRAM = install -m 644 -p
    QMAKE_INSTALL_FILE    = install -m 644 -p
}

win32:CONFIG(release, debug|release): LIBS += -lDataBaseWork
else:win32:CONFIG(debug, debug|release): LIBS += -lDataBaseWork
else:unix: LIBS += -lDataBaseWork

