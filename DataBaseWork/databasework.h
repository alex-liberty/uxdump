#ifndef DATABASEWORK_H
#define DATABASEWORK_H

#include "databasework_global.h"
#include <QtSql>
#include <QtSql/qsqlquery.h>
#include <QStringList>
#include <QSettings>
#include <QDebug>
#include <QDir>

/**
 * @brief The DataBaseWork class
 * The class for working and administrator database "UXDump", using driver QMYSQL or QSQLIGHT
 */
class DATABASEWORKSHARED_EXPORT DataBaseWork
{

public:
    DataBaseWork();
    ~DataBaseWork();

    void connectBaseSqlight();
    void connectBaseMySQL();

    void createBase();

    bool startTransaction();
    bool finishTransaction();


    bool lockTables();
    bool unlockTables();

    bool loadFromFile();

    int selectPersonId ( QString inputLogin );
    int selectTestId ( QString inputName );
    int selectSoftwareId ( QString inputName );
    int selectTestSoftwareId ();
    int selectMeasuringModuleId ( QString inputName );
    int selectParameter ( QString inputName );


    int addPerson ( QString login, QString sex, int YOB );
    int addTest (  QString name );
    int addSoftware ( QString name );
    int addTestSoftware();
    int addMeasuringModule ( QString name );
    int addLog ( int person, int testSoftware, int measuringModule, QString dateTime, int duration, QString note);
    int addLog ( QString person, QString sex, int YOB, QString test, QString software,
                 QString measuringModule, QString dateTime, int duration, QString note );
    int addParameter ( QString name );
    int addValue ( int log, QString parameter, QString data );

    QStringList selectPersonLogin ();
    QStringList selectTestName ();
    QStringList selectSoftwareName ();
    QStringList selectDistinctSoftwareName ();
    QStringList selectSoftwareName ( QString inputTestName );
    QStringList selectMeasuringModuleName ();
    QStringList selectDistinctDateTime();
    QStringList selectDateTime ( int inputLog );
    QStringList selectDistinctParameterName();

    QList<QStringList> selectValue( QString measuringModuleInput, QString testInput, QString softwareInput,
                                    QString personInput, QString parameterInput, QString dateTimeInput );
    bool deleteLog ( int inputId );

    QString selectPersonLogin ( int idInput );
    QString selectPersonSex ( QString personLogin);
    QString selectPersonYOB ( QString personLogin);

    QString selectTestName ( int idInput );
    QString selectSoftwareName ( int idInput );
    QString selectTestUsingSoftware ( int idInput );
    QString selectMeasuringModuleName ( int idInput );
    QString selectParameterName ( int idInput );

    int selectLog ( QString inputModule, QString inputTest, QString inputSoftware, QString inputLogin, QString inputDateTime );
    QList <int> selectLog ( QString inputModule, QString inputTest, QString inputSoftware, QString inputLogin );

    int maxValueId ();
private:

    QString hostName;
    QString userName;
    QString password;
    int port;
    QString dbaseName;

    void parserIni();

    int maxPersonId ();
    int maxTestId ();
    int maxSoftwareID ();
    int maxTestSoftwareID ();
    int maxMeasuringModuleId ();
    int maxLogId ();
    int maxParameterId ();



    QList <int> selectSoftwareUsingTest ( int inputId );

    QList <int> selectLogUsingPerson ( int inputId );
    QList <int> selectLogUsingTest ( int inputId );
    QList <int> selectLogUsingSoftware ( int inputId );
    QList <int> selectLogUsingModule ( int inputId );

    QList <int> selectParameterUsingLog ( int inputId );
    QList <int> selectDataUsingLog ( int inputId );


    bool updatePersonId();
    bool updateTestId();
    bool updateSoftwareId();
    bool updateMeasuringModuleId();
    bool updateLogId();
    bool updateParameterId();
    bool updateValueId();

    bool deletePerson ( int inputId );
    bool deleteTest ( int inputId );
    bool deleteSoftware ( int inputId );
    bool deleteMeasuringModule ( int inputId );
    bool deleteParameter ( int inputId );
    bool deleteValue ( int inputId );

    int idPerson;
    int idTest;
    int idSoftware;
    int idTestSoftware;
    int idMeasuringModule;
    int idLog;
    int idParameter;
    int idValue;

    int newId;
    int oldId;

    int resultSelect;
    bool result;

    QSqlDatabase dbase;

    QString strUpdate;
    QString strSelect;
    QString queryString;

    QSqlQuery queryUpdate;
    QSqlQuery query;

    QStringList outputList;
};

#endif // DATABASEWORK_H
