UXDump Suite is a toolset to evaluate effectiveness of human-computer interaction by measuring physical state changes of the user at his/her interaction with software.

It automates such tasks as running tests, acquiring data from measuring devices, collecting them into central database. Parallel testing of several users is provided to speed up experiments. 
