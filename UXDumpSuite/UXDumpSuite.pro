#-------------------------------------------------
#
# Project created by QtCreator 2016-03-16T16:17:29
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = UXDumpSuite
TEMPLATE = app

INCLIDEPATH += ./qwt
LIBS += -lqwt
CONFIG += qwt

SOURCES += main.cpp\
        uxdumpsuite.cpp

HEADERS  += uxdumpsuite.h

FORMS    += \
    uxdumpsuite.ui

win32:CONFIG(release, debug|release): LIBS += -lDataBaseWork
else:win32:CONFIG(debug, debug|release): LIBS += -lDataBaseWork
else:unix: LIBS += -lDataBaseWork

RESOURCES += \
    res/res.qrc
