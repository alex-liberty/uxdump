#-------------------------------------------------
#
# Project created by QtCreator 2016-03-08T23:40:29
#
#-------------------------------------------------

QT       += core gui sql

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = addTest
TEMPLATE = app


SOURCES += main.cpp\
        addtest.cpp

HEADERS  += addtest.h

FORMS    += addtest.ui

RESOURCES += \
    res/res.qrc

win32:CONFIG(release, debug|release): LIBS += -lDataBaseWork
else:win32:CONFIG(debug, debug|release): LIBS += -lDataBaseWork
else:unix: LIBS += -lDataBaseWork

win32:CONFIG(release, debug|release): LIBS += -lloaderCSV
else:win32:CONFIG(debug, debug|release): LIBS += -lloaderCSV
else:unix: LIBS += -lloaderCSV
