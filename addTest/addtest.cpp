#include "addtest.h"
#include "ui_addtest.h"

AddTest::AddTest(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::AddTest)
{
    testStart = new QProcess();

    ui->setupUi(this);
    slash = QDir::separator();
    db = new DataBaseWork();
    db->connectBaseMySQL();
    createModuleList();
    createTestList();
    createFirstWindow();
}

AddTest::~AddTest()
{
    delete ui;
}

void AddTest::createModuleList()
{
    int countModule = 0;
    path = QDir::currentPath() + "/Modules";
    moduleDirectory = QDir( path );
    QStringList nameFilter;


#ifdef Q_OS_LINUX
    //linux code goes here
    path = QDir::currentPath() + slash + "Modules";
    moduleDirectory = QDir( path );
    //nameFilter << "*.sh";
    listModules = moduleDirectory.entryList( QDir::Files );//nameFilter, QDir::Files );

    foreach (QString moduleName, listModules) {
        QString pathModule = path + slash + moduleName;
        QFileInfo testFile( pathModule );

        if ( testFile.isExecutable() ) {
            createTreeModule( moduleName, countModule );
            countModule++;
        }

    }
#elif Q_OS_WIN32
    // windows code goes here
    path = QDir::currentPath() + slash + "Modules";
    moduleDirectory = QDir( path );
    nameFilter << "*.exe"<<".cmd";
    listModules = moduleDirectory.entryList( nameFilter, QDir::Files );


    foreach (QString moduleName, listModules) {
        QString pathModule = path + slash + moduleName;
        QFileInfo testFile( pathModule );

        if ( testFile.isExecutable() ) {
            createTreeModule( moduleName, countModule );
            countModule++;
        }
    }

#else
#error Platform not supported
#endif

}

void AddTest::createLoginList()
{
    ui->comboBoxLogin->clear();
    personLogins.clear();

    personLogins = db->selectPersonLogin();
    ui->comboBoxLogin->addItems( personLogins );
}

void AddTest::createTestList()
{
    ui->treeWidgetTest->clear();
    testList.clear();
    path = QDir::currentPath() + slash + "Tests";
    moduleDirectory = QDir( path );
    testList = moduleDirectory.entryList( QDir::Dirs);
    testList.removeFirst();
    testList.removeFirst();

    int countTest = 0;

#ifdef Q_OS_LINUX

    foreach (QString test, testList) {
        QString pathModule = path + slash + test;
        QFileInfo testFile( pathModule );
        if ( testFile.isExecutable() ) {
            if ( testFile.completeSuffix() == "" ||
                 testFile.completeSuffix() == "sh") {
                createTreeTest( test, countTest );
                countTest++;
            }
        }
    }

#elif Q_OS_WIN32

    foreach (QString test, testList) {
        QString pathModule = path + slash + test;
        QFileInfo testFile( pathModule );
        if ( testFile.isExecutable() ) {
            if ( testFile.completeSuffix() == "exe" ||
                 testFile.completeSuffix() == "cmd") {
                createTreeTest( test, countTest );
                countTest;
            }
        }
    }
#else
#error Platform not supported
#endif
}

void AddTest::createTreeTest(QString testName, int countTest)
{

    QTreeWidgetItem *topLevelItem=new QTreeWidgetItem(ui->treeWidgetTest);
    ui->treeWidgetTest->addTopLevelItem(topLevelItem);
    topLevelItem->setText( 0, testName );

    QString buttonName = "info";
    QString infoDir =path + slash + testName + slash;
    QDir infoDirectory = QDir(infoDir);
    QStringList nameFilter;
    nameFilter<<"info*";
    QStringList ListInfo = infoDirectory.entryList(nameFilter, QDir::Files);

    if (ListInfo.count() != 0) {
        QPushButton* btn = new QPushButton();
        btn->setObjectName( buttonName + QString::number( countTest ));
        btn->setFixedSize(25, 25);
        btn->setIcon(QIcon(":/help"));
        connect( btn, SIGNAL( clicked() ), SLOT( onHelpTestClicked() ) );
        ui->treeWidgetTest->setItemWidget(topLevelItem, 1, btn);
    }
}

void AddTest::createTreeModule( QString moduleName, int countModule )
{
    QString settingsName = "settings" + QString::number( countModule );
    QString helpName = "help" + QString::number( countModule );

    QTreeWidgetItem *topLevelItem=new QTreeWidgetItem(ui->treeWidgetModule);
    ui->treeWidgetModule->addTopLevelItem(topLevelItem);
    topLevelItem->setText( 0, moduleName );

    QStringList configFilter;
    configFilter << moduleName + ".conf";

    QStringList config;

    config = moduleDirectory.entryList(configFilter, QDir::Files);

    if(config.count() == 0)
    {
        QStringList secondList = moduleName.split(".");
        secondList.removeLast();

        QString filter;
        for(int i = 0; i < secondList.count(); i++){
            filter += secondList[i];
            if(i != secondList.count() - 1)
                filter += ".";
        }

        configFilter.clear();
        configFilter << filter + ".conf";
        config = moduleDirectory.entryList(configFilter, QDir::Files);

        if(config.count() != 0){
            QPushButton* btn = new QPushButton();
            btn->setFixedSize(25, 25);
            btn->setObjectName( settingsName );
            btn->setIcon(QIcon(":/settings"));
            connect( btn, SIGNAL( clicked() ), SLOT( onSettingsClicked() ) );
            ui->treeWidgetModule->setItemWidget(topLevelItem, 1, btn);
        }
    }

    else
    {
        QPushButton* btn = new QPushButton();
        btn->setFixedSize(25, 25);
        btn->setObjectName( settingsName );
        btn->setIcon(QIcon(":/settings"));
        connect( btn, SIGNAL( clicked() ), SLOT( onSettingsClicked() ) );
        ui->treeWidgetModule->setItemWidget(topLevelItem, 1, btn);
    }

    QPushButton* btn2 = new QPushButton();
    btn2->setFixedSize(25, 25);
    btn2->setObjectName( helpName );
    btn2->setIcon(QIcon(":/help"));
    connect( btn2, SIGNAL( clicked() ), SLOT( onHelpClicked() ) );

    ui->treeWidgetModule->setItemWidget(topLevelItem, 2, btn2);

}

void AddTest::createFirstWindow()
{
    this->setWindowTitle("Select measuring modules (one or more)");
    ui->pushButtonNext1->setVisible( true );
    ui->pushButtonNext1->setEnabled( false );
    ui->labelHelp->setVisible( true );
    ui->treeWidgetModule->setVisible( true );
    ui->pushButton->setVisible( true );

    ui->treeWidgetModule->header()->setStretchLastSection( true );
    ui->treeWidgetModule->setColumnWidth( 0, 400);
    ui->treeWidgetModule->setColumnWidth( 1, 80 );
    ui->treeWidgetModule->setColumnWidth( 2, 80 );
    ui->treeWidgetModule->setSelectionMode(QAbstractItemView::MultiSelection);

    ui->pushButtonNext2->setVisible(false);
    ui->pushButtonBack2->setVisible(false);
    ui->treeWidgetTest->setVisible(false);

    ui->pushButtonRun->setVisible(false);
    ui->pushButtonBack3->setVisible(false);

    ui->labelSoftware->setVisible(false);
    ui->comboBoxSoftware->setVisible(false);

    ui->comboBoxLogin->setVisible(false);
    ui->labelLogin->setVisible(false);
    ui->labelSex->setVisible(false);
    ui->comboBoxSex->setVisible(false);
    ui->labelYOB->setVisible(false);
    ui->lineEditYOB->setVisible(false);
    ui->labelNote->setVisible(false);
    ui->lineEditNote->setVisible(false);
}

void AddTest::createSecondWindow()
{
    testList.clear();
    this->setWindowTitle("Select test module");
    ui->pushButtonNext1->setVisible(false);
    ui->treeWidgetModule->setVisible(false);
    ui->pushButton->setVisible(false);

    ui->pushButtonNext2->setVisible(true);
    ui->pushButtonNext2->setEnabled( false );
    ui->labelHelp->setVisible( true );
    ui->pushButtonBack2->setVisible(true);
    ui->treeWidgetTest->setVisible(true);

    ui->treeWidgetTest->setColumnWidth( 0, 500);
    ui->treeWidgetTest->setColumnWidth( 1, 80 );

    ui->pushButtonRun->setVisible(false);
    ui->pushButtonBack3->setVisible(false);

    ui->labelSoftware->setVisible(false);
    ui->comboBoxSoftware->setVisible(false);
    // ui->labelHelp->setVisible(true);

    ui->comboBoxLogin->setVisible(false);
    ui->labelLogin->setVisible(false);
    ui->labelSex->setVisible(false);
    ui->comboBoxSex->setVisible(false);
    ui->labelYOB->setVisible(false);
    ui->lineEditYOB->setVisible(false);
    ui->labelNote->setVisible(false);
    ui->lineEditNote->setVisible(false);

}

void AddTest::createThirdWindow()
{
    this->setWindowTitle("Fill in data");
    ui->pushButtonNext1->setVisible(false);
    ui->treeWidgetModule->setVisible(false);

    ui->pushButtonNext2->setVisible(false);
    ui->pushButtonBack2->setVisible(false);
    ui->treeWidgetTest->setVisible(false);

    ui->pushButtonRun->setVisible(true);
    ui->pushButtonBack3->setVisible(true);

    ui->labelSoftware->setVisible(true);
    ui->comboBoxSoftware->setVisible(true);
    //ui->labelHelp->setVisible(false);

    ui->comboBoxLogin->setVisible(true);
    ui->labelLogin->setVisible(true);
    ui->labelSex->setVisible(true);
    ui->comboBoxSex->setVisible(true);
    ui->labelYOB->setVisible(true);
    ui->lineEditYOB->setVisible(true);
    ui->labelNote->setVisible(true);
    ui->lineEditNote->setVisible(true);
    createSoftwareList();
    createLoginList();
}

void AddTest::createSoftwareList()
{
    test = ui->treeWidgetTest->currentItem()->text( 0 );
    path = QDir::currentPath() + slash + "Tests" + slash + test;
    softwareDirectory = QDir( path );
    QStringList nameFilter;
    QString name;
    nameFilter<<"start_*";
    QStringList softwares = softwareDirectory.entryList(nameFilter, QDir::Files);

    foreach (QString software, softwares) {
        QStringList softwareFirstSplit = software.split( "start_" );
        QStringList softwareSecondSplit = softwareFirstSplit[ 1 ].split( "." );
        softwareSecondSplit.removeLast();
        if ( softwareSecondSplit.count() == 0 )
            softwareList.append( softwareFirstSplit[ 1 ]);
        if ( softwareSecondSplit.count() == 1 )
            softwareList.append( softwareSecondSplit[ 0 ] );
        else
            for ( int i = 0; i <  softwareSecondSplit.count(); i++ ) {
                name += softwareSecondSplit[ i ];
                if ( ! (i == softwareSecondSplit.count() - 1 ) ) {
                    name += " ";
                    continue;
                }
                softwareList.append( name );
            }
    }

    ui->comboBoxSoftware->addItems( softwareList );
}

void AddTest::createDir()
{
    QString dirName;
    path = QDir::currentPath();
    dirName = "Result";
    isExsist = QDir( path + slash + "Result" ).exists();
    QDir dir = QDir( path );

    if( !isExsist )
        isExsist = dir.mkdir( dirName );

    dirName = "LogsAfterAdd";
    isExsist = QDir( path + slash + "LogsAfterAdd" ).exists();
    dir = QDir( path );

    if( !isExsist )
        isExsist = dir.mkdir( dirName );
}

void AddTest::readData()
{
    date = new QDate();
    int currentYear = date->currentDate().year();
    int lowYear = currentYear - 128;

    YOB = ui->lineEditYOB->text().toInt();

    if ( YOB < lowYear || YOB > currentYear) {

        QMessageBox::warning(this, "Error",
                             "Некорректное заполнение поля YOB, возраст должен быть от 1 до 128");

        return;
    }

    test = ui->treeWidgetTest->currentItem()->text( 0 );
    software = ui->comboBoxSoftware->currentText();
    personLogin = ui->comboBoxLogin->currentText();
    sex = ui->comboBoxSex->currentText();
    note = ui->lineEditNote->text();
    measuringModules.clear();
    QList <QTreeWidgetItem*> modulesSelect = ui->treeWidgetModule->selectedItems();

    foreach (QTreeWidgetItem* moduleSelect, modulesSelect) {
        measuringModules.append( moduleSelect->text( 0 ) );
    }

    runSelected();


    timeFinish = QTime::currentTime();
    duration = timeStart.msecsTo( timeFinish );

    foreach (QString moduleName, measuringModules) {
        createLog( moduleName );
    }
    this->close();

}

void AddTest::runSelected()
{
    dateTime = QDateTime::currentDateTime().toString( "dd/MM/yyyy hh:mm:ss:zzz" );

    foreach( QString module, measuringModules ) {
        QProcess *moduleStart = new QProcess();
        processes.append( moduleStart );
    }

    for ( int i = 0; i < measuringModules.count(); i++ ) {
        QString pathStart = QDir::currentPath() + slash + "Modules" + slash + measuringModules[ i ];
        processes[ i ]->start( pathStart );
    }

    QString testPath = QDir::currentPath() + slash + "Tests" + slash + test + slash;
    QDir *testDir = new QDir( testPath );

    QStringList nameFilter;
    nameFilter<<"start_" + software + "*";
    QStringList tests = testDir->entryList(nameFilter, QDir::Files);

    timeStart = QTime::currentTime();
    testStart->start( testPath + tests[ 0 ] );

    testStart->waitForFinished( -1 );

    foreach (QProcess *moduleFinish, processes) {
        moduleFinish->terminate();
        moduleFinish->waitForFinished( -1 );
    }
}

void AddTest::on_processFinished()
{   qDebug() << "sdlkfjsdkljflsdjfklsd\n";
    //  we have just noticed our testing module have finished
    timeFinish = QTime::currentTime();
    duration = timeStart.msecsTo( timeFinish );
    foreach (QProcess *moduleFinish, processes) {
        //а вот фиг, не убивается оно до конца... бяка такая
        moduleFinish->terminate();
        //moduleFinish->waitForFinished();

        // moduleFinish->kill();

    }
    foreach (QString moduleName, measuringModules) {
        createLog( moduleName );
    }
    this->close();
}

void AddTest::createLog(QString moduleName )
{
    logId = db->addLog( personLogin, sex, YOB, test, software, moduleName, dateTime, duration, note );
    parserData( moduleName );
}

void AddTest::parserData( QString moduleName )
{
    QStringList filterLog;
    filterLog << "*." + moduleName +".csv";
    path = QDir::currentPath() + slash + "Result" + slash;
    logDirectory = QDir( path );

    logs = logDirectory.entryList( filterLog, QDir::Files );
    foreach (QString log, logs) {
        QString fileName = path + log;
        if ( fileName == "" )
            return ;
        QFile *file = new QFile( fileName );
        QThread::sleep(10);
        while( file->size() == 0 ) {

        }
        csv = new LoaderCSV();
        csv->CSVReader( logId, fileName );

        logDirectoryAfterAdd = QDir::currentPath() + slash + "LogsAfterAdd";
        proc = new QProcess();
        proc->start( "mv " + fileName + " " + logDirectoryAfterAdd );
        proc->finished( 1 );
    }
}

void AddTest::on_pushButtonNext1_clicked()
{
    createSecondWindow();
}

void AddTest::on_comboBoxLogin_currentTextChanged(const QString &arg1)
{
    ui->comboBoxSex->setEnabled( true );
    ui->lineEditYOB->setEnabled( true );
    ui->lineEditYOB->setText( "" );

    sex = db->selectPersonSex( arg1 );
    if ( sex != "" ) {
        ui->comboBoxSex->setCurrentText( sex );
        ui->comboBoxSex->setEnabled( false );
    }

    QString YOBStr = db->selectPersonYOB( arg1 );
    if ( YOBStr != "" ) {
        ui->lineEditYOB->setText( YOBStr );
        ui->lineEditYOB->setEnabled( false );
    }
}

void AddTest::on_pushButtonRun_clicked()
{
    createDir();
    readData();
}

void AddTest::on_pushButtonNext2_clicked()
{
    createThirdWindow();
}

void AddTest::on_pushButtonBack2_clicked()
{
    createFirstWindow();

}

void AddTest::on_pushButtonBack3_clicked()
{
    createSecondWindow();
}

void AddTest::onSettingsClicked()
{
    QStringList splitName;
    QString text;

    if ( QPushButton* btn = qobject_cast< QPushButton* >( sender() ) ) {
        QString name = btn->objectName();
        splitName = name.split("settings");
        int row = splitName[1].toInt();

        if ( !row )
            text = ui->treeWidgetModule->itemAt(0, 0)->text(0);

        else {
            QTreeWidgetItem *prevItem = ui->treeWidgetModule->itemAt( 0, 0 );
            for ( int i = 0; i < row; i++ ) {
                prevItem = ui->treeWidgetModule->itemBelow( prevItem );
                text = prevItem->text( 0 );
            }
        }

        QString conf = text + ".conf";
        QString filePath = QDir::currentPath() + "/Modules/" + conf;

        confOpen = new QProcess();
        confOpen->start( "gedit " + filePath );

        if ( !confOpen->waitForFinished(30000000) )
            confOpen->kill();

        return;
    }
}

void AddTest::onHelpClicked()
{
    QStringList splitName;
    QString text;

    if ( QPushButton* btn = qobject_cast< QPushButton* >( sender() ) ) {
        QString name = btn->objectName();
        splitName = name.split("help");
        int row = splitName[1].toInt();

        if ( !row )
            text = ui->treeWidgetModule->itemAt(0, 0)->text(0);

        else {
            QTreeWidgetItem *prevItem = ui->treeWidgetModule->itemAt( 0, 0 );
            for ( int i = 0; i < row; i++ ) {
                prevItem = ui->treeWidgetModule->itemBelow( prevItem );
                text = prevItem->text( 0 );
            }
        }

        QString filePath =  "." + slash + "Modules" + slash + text;

        confOpen = new QProcess();
        QStringList arguments;
        arguments << "-h";
        confOpen->start( filePath, arguments);
        confOpen->waitForReadyRead(100);
        QString helpStr = confOpen->readAllStandardOutput();
        QMessageBox::information( this, "Info " + text, helpStr );
        confOpen->finished(1);

        return;
    }
}

void AddTest::onHelpTestClicked()
{

    QStringList splitName;
    QString text;

    if ( QPushButton* btn = qobject_cast< QPushButton* >( sender() ) ) {
        QString name = btn->objectName();
        splitName = name.split("info");
        int row = splitName[1].toInt();

        if ( !row )
            text = ui->treeWidgetTest->itemAt(0, 0)->text(0);

        else {
            QTreeWidgetItem *prevItem = ui->treeWidgetTest->itemAt( 0, 0 );
            for ( int i = 0; i < row; i++ ) {
                prevItem = ui->treeWidgetTest->itemBelow( prevItem );
                text = prevItem->text( 0 );
            }
        }

        QString filePath = QDir::currentPath() + slash + "Tests" + slash + text + slash;
        QByteArray data;
        QDir infoDirectory = QDir(filePath);
        QStringList nameFilter;
        nameFilter<<"info*";
        QStringList ListInfo = infoDirectory.entryList(nameFilter, QDir::Files);
        QFile file( filePath + ListInfo[ 0 ] );

        if ( !file.open( QIODevice::ReadOnly ) )
            return;

        data = file.readAll();
        QMessageBox::information( this, "Info " + text, QString(data) );
        return;
    }
}


void AddTest::on_treeWidgetModule_clicked(const QModelIndex &index)
{
    if ( ui->treeWidgetModule->selectedItems().count() != 0 ) {
        ui->labelHelp->setVisible( false );
        ui->pushButtonNext1->setEnabled( true );
    }

    else {
        ui->labelHelp->setVisible( true );
        ui->pushButtonNext1->setEnabled( false );
    }
}

void AddTest::on_treeWidgetTest_clicked(const QModelIndex &index)
{
    if ( ui->treeWidgetModule->selectedItems().count() != 0 ) {
        ui->labelHelp->setVisible( false );
        ui->pushButtonNext2->setEnabled( true );
    }

    else {
        ui->labelHelp->setVisible( true );
        ui->pushButtonNext2->setEnabled( false );
    }
}
